﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedditMVC.Models
{
    public class WorldNewsModel
    {
        public int Id { get; set; }
        public int WhichView { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Score { get; set; }
        public string Date { get; set; }
        public string Flair { get; set; }
    }
}
