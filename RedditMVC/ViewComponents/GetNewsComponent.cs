﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RedditMVC.Models;
using System.Net.Http;
using Newtonsoft.Json;

namespace RedditMVC.ViewComponents
{
    [ViewComponent(Name = "GetNews")]
    public class GetNewsComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(string newsId)
        {
            var model = await GetItemsAsync(Int32.Parse(newsId));
            return View(model);
        }

        private async Task<WorldNewsModel> GetItemsAsync(int tempId)
        {
            string newsfeed = string.Empty;
            using (var client = new HttpClient())
            {
                WorldNewsModel resultitems = new WorldNewsModel();
                try
                {
                    var response = client.GetAsync("http://localhost:8000/api/WorldNews/" + tempId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = response.Content;
                        newsfeed = responseContent.ReadAsStringAsync().Result;
                    }
                    resultitems = JsonConvert.DeserializeObject<WorldNewsModel>(newsfeed);
                }
                catch (Exception e)
                { }

                return resultitems;
            }
        }
    }
}
