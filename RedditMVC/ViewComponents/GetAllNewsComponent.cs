﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RedditMVC.Models;
using System.Net.Http;
using Newtonsoft.Json;

namespace RedditMVC.ViewComponents
{
    [ViewComponent(Name = "GetAllNews")]
    public class GetAllNewsComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(int whView)
        {
            var model = await GetAllItemsAsync();
            if (whView == 2)
            {
                return View("Carousel", model);
            }
            else
            {
                return View(model);
            }
        }

        private async Task<List<WorldNewsModel>> GetAllItemsAsync()
        {
            string newsfeed = string.Empty;

            using (var client = new HttpClient())
            {
                List<WorldNewsModel> resultitems = new List<WorldNewsModel>();
                try
                {
                    var response = client.GetAsync("http://localhost:8000/api/WorldNews/").Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = response.Content;
                        newsfeed = responseContent.ReadAsStringAsync().Result;
                    }
                    resultitems = JsonConvert.DeserializeObject<List<WorldNewsModel>>(newsfeed);
                }
                catch (Exception e)
                { }
                
                return resultitems;
            }
        }
    }
}
